import { createHeaders } from './index'

const apiUrl = process.env.REACT_APP_API_URL

//Asyncronus function to check if user exsist
const checkForUser = async (username) => {
    try {
        const response = await fetch(`${apiUrl}?username=${username}`) //Gets the api url from the .env file and checks if the username already exists
        if (!response.ok) {
            throw new Error('Could not complete request.')
        }
        const data = await response.json() //If it runs with success get the data from the response.json
        return [ null, data ]

    } catch (error) {
        return [ error.message, [] ]
    }
}

//Function to create new user
const createUser = async(username) => {
    try {
        const response = await fetch(apiUrl, {
            method: 'POST',                     //Tell the server that we want to create a new resource
            headers: createHeaders(),           //Function that returns the new object that will become the headers
            body: JSON.stringify({              //Cant send object with fetch request. Turns object that we create and turn it into a string
                username,
                translations: []
            })
        } ) 
        if (!response.ok) {
            throw new Error('Could not create a user with the username ' + username)
        }
        const data = await response.json() //If it runs with success get the data from the response.json. Responds with the user
        return [ null, data ]

    } catch (error) {
        return [ error.message, [] ]
    }
}

export const loginUser = async (username) => {
    const [ checkError, user ] = await checkForUser(username)

    if (checkError !== null) {
        return [checkError, null]
    }

    if (user.length > 0) { //if user exist
        return [ null, user.pop() ]
    }

    return await createUser(username)

    

}