import { createContext, useState, useContext } from "react";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { storageRead } from "../utils/storage";

//Responsible for managing the user

//Context relies on two parts

// 1. The context object - exposing the state
const UserContext = createContext()

export const useUser = () => {
    return useContext(UserContext) //Returns object { user and setUser function }
}


// 2. Provider - managing the state
const UserProvider = ({ children }) => {

    const [ user, setUser ] = useState( storageRead( STORAGE_KEY_USER ) )

    //Create state
    const state = {
        user,
        setUser
    }

    return (
        <UserContext.Provider value={ state }>
            { children }
        </UserContext.Provider>
    )
}
export default UserProvider

