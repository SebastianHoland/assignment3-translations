import UserProvider from "./UserContext"
//Responsible for merging all the contexts that we might have together

const AppContext = ({ children }) => {
    
    return(
        
        <UserProvider>
            { children }
        </UserProvider>
        
    )

}

export default AppContext