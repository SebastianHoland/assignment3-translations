import { useState, useEffect } from "react";
import { useForm } from 'react-hook-form' //To use react-hook-form
import { loginUser } from '../../api/user' //Import the loginUser function that is created in the api file
import { storageSave } from '../../utils/storage' //Import storageSave from storage.js
import { useNavigate } from 'react-router-dom'
import { useUser } from '../../context/UserContext'
import { STORAGE_KEY_USER } from '../../const/storageKeys'

const usernameConfig = {
    required: true,
    minLength: 3
}

//Function component
const LoginForm = () => {
    //Hooks
    const { register, handleSubmit, formState: { errors } } = useForm ()
    const { user, setUser } = useUser()
    const navigate = useNavigate()

    //Local state
    const [ loading, setLoading ] = useState(false)
    const [ apiError, setApiError ] = useState(null)

    //Side Effects
    useEffect(() => {
        if (user !== null) {
            navigate('/profile')
        }
    }, [ user, navigate ] ) //Empty dependencies only run once
    

    //Event Handlers
    const onSubmit = async ({ username }) => { //Destructure the username from the callback functions argument list
        setLoading(true);
        const [error, userResponse] = await loginUser(username)
        if (error !== null) {
            setApiError(error)
        }
        //Store user in local storage if login is successful
        if (userResponse !== null) {
            storageSave(STORAGE_KEY_USER , userResponse)
            setUser(userResponse)
        }
        setLoading(false);
    };

        //Render Functions
        const errorMessage = (() => {
            if (!errors.username){ 
                return null
            }

            if (errors.username.type === 'required' ) {
                return <span>Username is required</span>
            }

            if (errors.username.type === 'minLength') {
                return <span>Username is too short (at least 3 characters required)</span>
            }
        })()

    return (
        <>
            <h2>What is you name?</h2>
            <form onSubmit={ handleSubmit(onSubmit) }>
                <fieldset>
                    <label htmlFor="username">Username: </label>
                    <input 
                    type="text" 
                    placeholder="Ola Nordmann" 
                    { ...register("username", usernameConfig) } 
                />
                { errorMessage }
                
                </fieldset>

                <button type="submit" disabled={ loading }>Continue</button>

                { loading && <p>Logging in...</p> }
                { apiError && <p>{ apiError }</p> }
            </form>

        </>
    )
}
export default LoginForm