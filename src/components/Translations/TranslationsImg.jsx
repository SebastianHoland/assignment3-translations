const TranslationsImg = ({ name, image }) => { //Takes two arguments in the form of name of the sign and image of the sign
    return (
        <button>
            <aside>
                <img src={ image } alt={ name } width="55"/>
            </aside>
            {/* Shows the name of the image file on the image */}
            <section>
                <b>{ name }</b>
            </section>
        </button>
    )
}

export default TranslationsImg