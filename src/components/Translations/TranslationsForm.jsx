import { useForm } from "react-hook-form"

const TranslationsForm = ({ onTranslate }) => {

    const { register, handleSubmit } = useForm()

    const onSubmit = ({ translate }) => { onTranslate(translate) }

    return (
        <form onSubmit={handleSubmit(onSubmit) }>
            <fieldset>
              {/* The text input needs to be linked to the sign language images. How do I do that?????? */} 
                <label htmlFor="translate">Type in what you want to translate: </label>
                <input type="char" { ...register('translate') } placeholder="hello world" />
            </fieldset>
            <br></br>
            <button type="submit">Translate</button>
            
        </form>
    )
}
export default TranslationsForm