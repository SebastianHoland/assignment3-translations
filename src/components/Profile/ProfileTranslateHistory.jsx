import ProfileTranslationHistoryItem from "./ProfileTranlationHistoryItem"

const ProfileTranslationsHistory = ({ translations }) => {

    const translationsList = translations.map(
        (translation, index) => <ProfileTranslationHistoryItem key={ index + '_' + translation} translation={ translation } />
    )

    return (
        <section> 
            <h4>Your translation history</h4>
            <ul>
              { translationsList }  
            </ul>
        </section>
    )
}
export default ProfileTranslationsHistory