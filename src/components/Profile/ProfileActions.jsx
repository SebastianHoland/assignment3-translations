import { Link } from "react-router-dom"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../context/UserContext"
import { storageDelete } from "../../utils/storage"


//Component
const ProfileActions = () => {

    const { setUser } = useUser()

    //Handler function
    const handleLogoutClick = () => {
        if (window.confirm('Are you sure?')) {
            storageDelete(STORAGE_KEY_USER)
            setUser(null) //clearing the login

        }
    }

    return (
       <ul>
           <li><Link to="/translations">Translations</Link></li>
           <li><button>Clear history</button></li>
           <li><button onClick={ handleLogoutClick }>Logout</button></li>
       </ul>
    )

}
export default ProfileActions