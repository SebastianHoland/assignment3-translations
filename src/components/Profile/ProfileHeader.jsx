const ProfileHeader = ({ username }) => { //Get the user using destructuring from the props list
    return (
        <header>
            <h4>Hello {username}, welcome back to the world of signlanguage translations! Happy translations!</h4>
        </header>
    )
}
export default ProfileHeader